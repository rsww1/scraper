import pandas as pd
import os
from tqdm import tqdm


if __name__ == "__main__":
	missed_files = 0
	files_dir = "./csharp_files/"
	codes = []
	file_names = []
	for i, file_name in enumerate(tqdm(os.listdir(files_dir)),1):
		try:
			with open(files_dir + file_name) as csharp_file:
				code = csharp_file.read()
			codes.append(code)
			file_names.append(file_name)
		except Exception as err:
			missed_files  += 1

		if len(file_names) == 32000:
			df = pd.DataFrame({"file_name": file_names, "code": codes})
			df.to_parquet(f"./parquets/csharp_files{i}.parquet", engine="fastparquet")
			file_names = []
			codes = []

	df = pd.DataFrame({"file_name": file_names, "code": codes})
	df.to_parquet(f"./parquets/csharp_files{i+1}.parquet", engine="fastparquet")

	print(f"Files with wrong encoding {missed_files}")
