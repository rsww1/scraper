import os
import multiprocessing as mp
import subprocess
import json
import shlex

def run(repo_name):
    repo_url = f"https://github.com/{repo_name}.git"

    subprocess.run(["git", "clone", "--filter=blob:none", "--depth=1", repo_url], cwd="./repos/")
    repo_path = "./repos/"+repo_name.split("/")[1]
    subprocess.run(args=shlex.split('find . -type f ! -name "*.cs" -delete'), cwd=repo_path)

def safe_run(*args, **kwargs):
    """Call run(), catch exceptions."""
    try: run(*args, **kwargs)
    except Exception as e:
        print("error: %s run(*%r, **%r)" % (e, args, kwargs))

def main():
    # populate files
    repos_file = "./repo_urls/20002_repos.json"
    with open(repos_file) as f:
        repos_list = json.load(f)

    repos_list = [repo for repo in repos_list if not repo.split("/")[1] in os.listdir("./repos/")]
    NUM_OF_PROCESSES = 8
    pool = mp.Pool(NUM_OF_PROCESSES) # use all available CPUs
    pool.map(safe_run, repos_list)

if __name__=="__main__":
    mp.freeze_support() # optional if the program is not frozen
    main()

