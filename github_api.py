from github import Github
import json
from time import sleep
from datetime import timedelta, date


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


def query_wrapper(query):
    gen = iter(query)
    while True:
        try:
            yield next(gen)
        except StopIteration:
            break
        except Exception as e:
            sleep(60)


if __name__ == "__main__"

g = Github(per_page=100)

# 10 requests per minute
repos_list = []
number_of_repos = 0
first_date = date(2015, 5, 1)
last_date = date(2021, 1, 1)
save_interval = 10000

for single_date in daterange(first_date, last_date):

    start_date = single_date.strftime("%Y-%m-%d")
    end_date = (single_date + timedelta(1)).strftime("%Y-%m-%d")

    query = f'language:c# created:{start_date}..{end_date}'
    repositories = g.search_repositories(query=query)
    for repo in query_wrapper(repositories):
        repos_list.append(repo.full_name)
        number_of_repos += 1
        if len(repos_list) > save_interval:
            with open(f"{number_of_repos}_repos.json", "w") as f:  
                json.dump(repos_list, f)
                repos_list = []


    print(number_of_repos)
    
